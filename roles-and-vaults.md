# Roles and Vaults

## Convert the web-server playbook into a role

When a playbook is 'done', when it performs some function well, reliably, and idempotently, it may be time to fold it up into a role.

Roles are modularized Ansible code, settings, and config. Each module should do one thing well.

Roles can have default variables and combine with group and host variables to configure machines appropriately to their region, network location, or whatever.

```bash
# Create role directory tree
mkdir -p roles/webserver/{tasks,templates,defaults}
# Create role webpage template
cp -v index.html.j2 roles/webserver/templates/
# Set *default* webpage title
echo title: '"this is the default title"' > roles/webserver/defaults/main.yml
# Rough in the role tasks by copying in apache trifecta
cp -v apache-trifecta.yml roles/webserver/tasks/main.yml
# Look at the result
tree roles/ # or ls -lR roles/
```

Create configure webservers from apache trifecta

```bash
cp -v apache-trifecta.yml configure-webservers.yml
```

Edit [roles/webserver/tasks/main.yml](roles/webserver/tasks/main.yml)
- Remove play-level information from the webserver role's tasks

Edit [configure-webservers.yml](playbooks/configure-webservers.yml)
- Remove the tasks block from configure-webservers.yml
- Add a roles block to configure-webservers.yml

Run the new playbook!

```bash
ansible-playbook configure-webservers.yml -K
curl $FQDN_OR_IP
# examples:
# curl b3ab25b1691c.mylabserver.com
# curl 172.31.124.63
```

Let's change the site content:
1. Change the template in roles/webserver/templates
2. Re-run the playbook
3. Re-download the page with curl or Firefox

Let's play with variable definition precedence:
1. Remove and/or change the value of `title` in host_vars, group_vars, and defaults
2. Re-run the playbook
3. Re-download the page and predict the content
4. When you can accurately state precedence, you win!

## Create a vault for secret values

Often, we need to implant secrets onto machines, but we must not store unencrypted secrets in git.

One solution is to encrypt the secrets. (Another is to store references to secrets in a 3rd system, but that is out of scope for this fundies class.)

In this very simple exercise, we'll add a fake 'dbpass' variable to an Ansible vault.

```bash
# view vars for one of your managed nodes
ansible-inventory --host $SOME_HOST
# Setup the vars and vault pattern for webservers
# Create a directory for vars files for webservers
mkdir -p group_vars/webservers
# Create an Ansible vault with some secrets in it
touch group_vars/webservers/vault.yml
ansible-vault encrypt group_vars/webservers/vault.yml
# Choose a vault password that is easy for you to remember and type
# but hard for someone else to guess

# if you're comfortable with vim do this:
ansible-vault edit group_vars/webservers/vault.yml

# if you'd rather use another editor, do this:
ansible-vault decrypt group_vars/webservers/vault.yml
# edit with your preferred editor
ansible-vault encrypt group_vars/webservers/vault.yml

# Suggestion: Name your secret variables vault_something so you'll know where to look for them
# view the effect on the variables for one of your managed nodes
ansible-inventory --host $SOME_HOST
# notice that now, you can't view vars without giving the Ansible vault password
# tell Ansible to ask for the vault password
ansible-inventory --host $SOME_HOST --ask
```

View the content of group_vars/webservers/vault.yml with your viewer of choice. Notice that file is safe to add to version control, because the secrets are safely encrypted.

## Last words

### On Module Preference

It is best to find and use the right module for the job: package template service archive fetch copy user...

If you can't find a good module to solve the problem, here are some bad options that might help.

| Bad     | Worse | Worst | Worcestershire |
| ------- | ----- | ----- | -------------- |
| command | shell | raw   | script         |

### On Good Ansible Work in general

Good playbooks and roles are idempotent. If your playbook or role isn't idempotent, it isn't good.

Good changes are understood and endorsed by more than one person. If your change is only understood and/or endorsed by you, get a co-worker to review your code, give them a demo, and ask for their feedback.

Never commit unencrypted secrets to git. Not passwords. Not certificates. Not ssh keys. Encrypt them.

Never expose secrets where they are not needed.

Rotate shared secrets regularly.

### On variable placement and precedence

Put variable definitions in the right place.
- Values which do not vary, but are used in multiple places in a playbook or role, should be defined in the playbook or in the role variables folder, respectively.
- Default values should be in the role defaults folder. For example: program_install_location might be set to '/opt/$SOME_PROGRAM' here.
- Over-ride values for a group of hosts should be defined in group_vars/$SOME_GROUP. For example: timezone should be set to 'Mountain' for hosts in the 'Colorado' group.
- Over-ride values for *over-lapping* groups should *only* be defined in the group that matters to the playbook or role. Resist the urge to over-generalize.
- Over-ride values which are unique per host should be defined in host_vars/$SOME_HOST. Example: As a new setting rolls out across a group, the setting might first appear on individual hosts before *moving* from the hosts to the group.

Keep variable precedence in mind: https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable
