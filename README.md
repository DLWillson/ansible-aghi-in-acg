# Ansible AGHI in ACG

## What is Ansible?

- Define: DSL, CM, idempotent
- Compare to Puppet, Chef, hand-made scripts, etc.
    * easy idempotence
    * highly readable if properly done, but this can be ruined
    * low requirements
- Ad-hoc Operations, Static Inventory, and Target Specification
- Modules: ping, fetch, and copy

## Playbooks and Variables

- Roles
- Group and Host Variables
- Vaults

- Modules:
    * User
    * Package
    * Files: Fetch, copy, and template
    * Service
    * Reboot
    * Stdout and the Debug module

## Things we won't get to

- Dynamic Inventory
- Blocks
- Handlers
