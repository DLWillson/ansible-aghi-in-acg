## Setting up on ACG Cloud 

### Create Machines

Create a Fedora Workstation (latest) for the Ansible Controller and two Rocky Linux 8 machines for server machines. All will be Managed Nodes.

You may use other distributions or versions, e.g. CentOS 7, but you'll need to make changes to playbooks and instructions. The dinosaur poop gets in the way. There are important behavioral differences between Fedora latest, Rocky 8, and CentOS 7.

### Create an inventory

Log in to the workstation machine and change the password.

Log in to the all other machines and change their passwords to match.

Add all the machines to an inventory file, using their public hostnames or private ip addresses. The public IP addresses change.

```ini
[workstation]
b3ab25b1691c.mylabserver.com
[webservers]
b3ab25b1692c.mylabserver.com
b3ab25b1693c.mylabserver.com
```

If you prefer to use nicknames or you run into name-resolution issues, you might want to write your inventory like this, instead.

```ini
[workstation]
acws ansible_host=172.31.124.63
[webservers]
web1 ansible_host=172.31.119.147
web2 ansible_host=172.31.121.230
```

Ensure that your ssh will accept the public keys of new hosts without stopping to ask.

```bash
echo StrictHostKeyChecking accept-new > ~/.ssh/config
chmod go-rwx ~/.ssh/config
```

Verify Ansible connectivity to all the machines, with a password.

```bash
ansible all -i ACG-inventory -m ping -k
```

You want your output to look like this:

[Ansible ping them all](successful-password-ping-all.PNG)

Create an ansible.cfg to use the inventory you built.

```ini
[defaults]
inventory=ACG-inventory
```

Here are a couple ways to do that:

```bash
cat << EOT > ansible.cfg
[defaults]
inventory=ACG-inventory
EOT
```

```bash
echo -e '[defaults]\ninventory=ACG-inventory' > ansible.cfg
```
