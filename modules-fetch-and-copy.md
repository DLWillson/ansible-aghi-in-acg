# Ansible Modules

Let's practice with modules. As we practice, notice that most modules are idempotent, they check whether something needs to be done before doing it.

Further reading: https://docs.ansible.com/ansible/2.9/modules/list_of_all_modules.html

### Ping

Create an inventory of a few real-world nodes. These should be nodes you can really reach with ssh from your Ansible Controller.

```bash
# create a small inventory of real nodes 
echo a-real-node > real
echo another-node >> real
echo yet-another-node >> real
cat real
```

Update ansible.cfg to use the new inventory

```ini
[defaults]
inventory=real
```

```bash
# use the ping module to ensure that Ansible can connect to all the nodes
ansible -m ping all -k
# in an env where keys work, `-k` is not needed
```

Troubleshoot as needed. You may need to start ssh-agent, modify ~/.ssh/config, and/or add `-k` to the ansible expression so that you can provide your password.

Inventory can also be created with IP addresses. If you are using A Cloud Guru, use the private IP addresses (172.x.x.x). The IP may be preferred over FQDN as FQDN may take some time to propigate.

### Fetch

Fetch fetches files (or directories) from the managed node/s.

Now, let's observe the idempotent behavior of the fetch module.

```bash
# fetch /etc/passwd and /etc/fstab from all
ansible all -m fetch -a "src=/etc/fstab dest=$PWD/fetched/" -k
ansible all -m fetch -a "src=/etc/passwd dest=$PWD/fetched/" -k

# see how nicely 'fetch' arranges the fetched files
tree fetched/
# if you don't have tree and don't want to install it, use "ls -lR"
```

Re-run the above expressions. Notice that when the src files haven't changed (i.e. they still match the files in dest), fetch doesn't re-download them. Observe the idempotence. Woo!

### Copy

Copy copies files (or directories) *to* the managed node/s.

Let's observe idempotent behavior in the copy module.

```bash
# Let's write a letter to Uncle Bob
echo 'Dear Uncle Bob,' > $USER.txt
# And send it to the managed nodes
ansible all -m copy -a "src=$PWD/$USER.txt dest=/tmp/" -k
# and re-run to show idempotence
ansible all -m copy -a "src=$PWD/$USER.txt dest=/tmp/" -k
# and update and re-run to show that it does what it should
echo "
Hope all is well with you and Aunt Alice.

Your adoring nephew,

$USER" >> $USER.txt
ansible all -m copy -a "src=$PWD/$USER.txt dest=/tmp/" -k
```

## Reinforcing Exercises

What do "SUCCESS", "CHANGED", and "FAILURE" mean,
in the output of an adhoc job?

Use the fetch module to fetch back your letter to Uncle Bob.

Update your letter to Uncle Bob and send it to a subset of nodes.
example: ansible web1:db0:files -m copy ...

Re-fetch from all nodes. What do you see?
