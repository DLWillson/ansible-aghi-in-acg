# What is Ansible?

It's a DSL (domain-specific language) for CM (configuration management), like Puppet, Salt, and Chef. It can also be used for IAC (infrastructure as code), like Terraform. Finally, it *excels* in ad-hoc operations on many nodes, like pssh, css, and synced panes in Terminator and tmux.

https://medium.com/successivetech/chef-vs-puppet-vs-ansible-vs-saltstack-a-complete-comparison-9af8f1790c0d

Its requirements at the Ansible Controller and Managed Node are much lower than competing systems. The amount of setup and learning one must do before turning Ansible to productive ends is minimal. Yet, it also can work well for policy enforcement and in networks with hundreds or thousands of managed nodes.

Ansible works well for ad-hoc operations like collecting configuration files, patch and reboot, complex orchestration operations like cascading restarts with health-checks, IAC operations like building networks and instances in clouds, and for regular CM operations like installing packages, configuring and starting services, and adding users.

Ansible modules, playbooks, and roles should be **idempotent**. This means at least that multiple runs of a playbook will produce the same result, but generally, it also means that the tool doesn't do work where none is needed, and reports three different exit states: "system already OK, no change needed", "configured the system", and "failed to configure the system". Modules, roles, and playbooks that always take action, even when no action is needed, should be not be considered "done". Modules (and playbooks) should always attempt to verify whether a managed node is already compliant with the desired policy before *making* it compliant.

Further Reading: [Ansible Basics](https://docs.ansible.com/ansible/latest/getting_started/index.html)


## Exercises

Perform these actions at any Ansible Controller. If you happen to be on a machine without Ansible installed, install it. *poof* Now, it's an Ansible Controller.

## Standalone

You can do most operations standalone to 'localhost'. Look ma, no inventory, no ssh, no net!

```
$ ansible localhost -m ping
localhost | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```

```
$ ansible localhost -m fetch -a "src=~/.bashrc dest=$PWD/fetched/"
localhost | CHANGED => {
    "changed": true,
    "checksum": "e15622a7565145b0a4c8d23e251d3f05ea8843d5",
    "dest": "/home/willsodl/fetched/localhost/home/willsodl/.bashrc",
    "md5sum": "375e2a2c9162ab2d9e5c96d789587034",
    "remote_checksum": "e15622a7565145b0a4c8d23e251d3f05ea8843d5",
    "remote_md5sum": null
}

$ tree -a fetched/localhost/
fetched/localhost/
`-- home
    `-- willsodl
        `-- .bashrc

2 directories, 0 files
```

```
$ ansible localhost -m synchronize -a "src=~/.ssh dest=$PWD/synched/"
localhost | CHANGED => {
    "changed": true,
    "cmd": "/usr/bin/rsync --delay-updates -F --compress --archive --out-format=<<CHANGED>>%i %n%L /home/willsodl/.ssh /home/willsodl/synched/",
    "msg": "cd+++++++++ .ssh/\n>f+++++++++ .ssh/authorized_keys\n>f+++++++++ .ssh/id_rsa\n>f+++++++++ .ssh/id_rsa.pub\n>f+++++++++ .ssh/known_hosts\n>f+++++++++ .ssh/known_hosts.old\n",
    "rc": 0,
    "stdout_lines": [
        "cd+++++++++ .ssh/",
        ">f+++++++++ .ssh/authorized_keys",
        ">f+++++++++ .ssh/id_rsa",
        ">f+++++++++ .ssh/id_rsa.pub",
        ">f+++++++++ .ssh/known_hosts",
        ">f+++++++++ .ssh/known_hosts.old"
    ]
}

$ tree -a synched/
synched/
`-- .ssh
    |-- authorized_keys
    |-- id_rsa
    |-- id_rsa.pub
    |-- known_hosts
    `-- known_hosts.old

1 directory, 5 files
```
