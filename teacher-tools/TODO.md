### To Do

Add using ansible-galaxy as a source of example roles.

Add blocks and handlers

Machines only need to be rebooted when... Update the pnr playbook to only reboot the machine when needed.

httpd only needs to be restarted when... Update the apache playbook and webserver role to only start/restart httpd when needed.

Add a loop to index.html.j2 and add something amusing
