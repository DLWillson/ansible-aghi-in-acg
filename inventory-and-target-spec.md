## Ansible Inventory

Create a fictitious inventory and play with it.

```bash
# Create an INI-style inventory file
for MNOG in balrog sauron [fellowship] gandalf [fellowship:children] dwarves elves hobbits humans [dwarves] gimli [elves] legolas [hobbits] frodo merry pippin sam [humans] aragorn boromir [nazgul] ringwraith[0:8]
do
    echo $MNOG >> lotr
done

# Check
cat lotr

# List Nodes
ansible -i lotr --list all
ansible -i lotr --list fellowship
ansible-inventory -i lotr --graph

# These two forms, JSON and YAML, can be used as inventory
# So, if you don't like INI style inventory, you can change
ansible-inventory -i lotr --list
ansible-inventory -i lotr --list --yaml
```

Inventory can be stored INI, JSON, YAML, dynamic, and combinations.

Further Reading: [Ansible Inventory](https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html)

Specify the inventory in `ansible.cfg` so that you don't have to keep specifying it as an argument.

Create this `ansible.cfg` however you like: vim, nano, echo with tee, or whatever.

```ini
[defaults]
inventory=lotr
```


## Targeting

Practice with target specifications

```bash
# Specify a group
ansible --list humans
ansible --list fellowship

# List short people
ansible --list dwarves:hobbits

# SPOILER: List the humans who survive
ansible --list 'humans:!boromir'
```
