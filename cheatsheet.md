# Cheatsheet

```bash
ansible --help
man ansible
```

## Commands

ansible

ansible-inventory

ansible-playbook

## Modules

fetch and copy

file

user

package

copy, template, lineinfile

service

reboot

command, shell, raw, script*
* dis-preferred. dispreference increases left to right