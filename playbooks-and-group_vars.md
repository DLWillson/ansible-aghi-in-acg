# Ansible Playbooks and Vars

# Ansible Playbooks

Playbooks are YAML-formatted lists of plays. Often, just one play, but that's not a rule, at all.

Further reading: https://github.com/sfromm/ansible-playbooks

### authorize your keypair on all the things

Create [authorize-my-key.yml](playbooks/authorize-my-key.yml) using your editor of choice.

```bash
# Create a keypair, if you haven't already
# choose a good encryption passphrase
ssh-keygen

# Authorize your keypair on all the machines
ansible-playbook authorize-my-key.yml -k
```

Verify that your ssh key pair has been authorized everywhere by using it to Ansible ping everything.

```bash
# decrypt the private key in memory for the session
eval $( ssh-agent )
ssh-add
# Ansible ping everything
ansible all -m ping
```

Notice that you don't have to provide a password or decrypt your private key over and over.

Read the playbook and explain what just happened.

### patch and reboot

Create [patch-and-reboot](playbooks/patch-and-reboot.yml) using your editor of choice.

Choose a target server and run it against that server using commands similar to the following.

Replace my server's name with your server's name!

```bash
# Target the playbook with an Ansible variable
ansible-playbook patch-and-reboot.yml --extra-vars="target=b3ab25b1693c.mylabserver.com" -K
ansible-playbook patch-and-reboot.yml --extra-vars="target=web1" -K

# Target the playbook with an environment variable
TARGET=b3ab25b1692c.mylabserver.com ansible-playbook patch-and-reboot.yml -K
TARGET=web1 ansible-playbook patch-and-reboot.yml -K
```


### Apache trifecta (package template service)

Create [apache-trifecta](playbooks/apache-trifecta.yml) and [index.html.j2](templates/index.html.j2) using your editor of choice.

Create your webservers!

```bash
ansible-playbook apache-trifecta.yml -K
```

View the webpages in a web browser or using curl, wget, or elinks.

```bash
curl b3ab25b1692c.mylabserver.com
firefox b3ab25b1692c.mylabserver.com
```

### Use a group var

Create /group_vars/webservers/vars.yml

```bash
mkdir group_vars/webservers -p
touch group_vars/webservers/vars.yml
echo "title: My Title" > group_vars/webservers/vars.yml
```

 and *move* the 'title' var to it and give it a better value.

Remove the 'title' var from the vars key or remove the whole vars key from the play.

Re-run the playbook. Re-browse the http servers using curl, links, or a graphical browser.

### Override a group var with a host var

Create /host_vars/$SOME_HOST/vars.yml and override the 'title' var in it.

Re-run the playbook.

Re-browse the http servers using curl, links, or a graphical browser.

Note that one target is unchanged and one is changed.

### Further Reading

https://linuxhandbook.com/jinja2-templates/

Use a loop in a template:
- [playbook](https://gitlab.com/JacobSayre/ja-linux-devops-classes-2023/-/blob/main/TeacherTools/configure-haproxy.yml)
- [template](https://gitlab.com/JacobSayre/ja-linux-devops-classes-2023/-/blob/main/TeacherTools/haproxy.cfg.j2) Look for {% %}
